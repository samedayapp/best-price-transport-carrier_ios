//
//  AvailableBidsVC.h
//  SDPACarrier
//
//  Created by Sapana Ranipa on 29/12/15.
//  Copyright © 2015 Elluminati. All rights reserved.
//

#import "BaseVC.h"
#import <CoreLocation/CoreLocation.h>

@interface AvailableBidsVC : BaseVC <UICollectionViewDataSource,UICollectionViewDelegate,UIAlertViewDelegate,UITableViewDelegate,UITableViewDataSource,CLLocationManagerDelegate,UITextFieldDelegate,UITextViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate>
{
    CLLocationManager *locationManager;
    NSTimer *timerForBids,*timerForPush;
}

@property (weak, nonatomic) IBOutlet UILabel *lblViewbids;
@property (weak, nonatomic) IBOutlet UILabel *lblUnderline;
@property (weak, nonatomic) IBOutlet UILabel *lblOperational;
@property (weak, nonatomic) IBOutlet UILabel *lblTransport;
@property (weak, nonatomic) IBOutlet UILabel *lblSetOperational;
@property (weak, nonatomic) IBOutlet UILabel *lblSetTransport;
@property (weak, nonatomic) IBOutlet UILabel *lblPickupDate;
@property (weak, nonatomic) IBOutlet UILabel *lblDeliverDate;


@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UIButton *btnDistance;
@property (weak, nonatomic) IBOutlet UIButton *btnPickupDate;
@property (weak, nonatomic) IBOutlet UIButton *btnDeliveryDate;


@property (weak, nonatomic) IBOutlet UIView *viewForMenu;
@property (weak, nonatomic) IBOutlet UICollectionView *CollectionObj;
@property (weak, nonatomic) IBOutlet UITableView *tableObj;
@property (weak, nonatomic) IBOutlet UITableView *tableForDistance;

@property (weak, nonatomic) IBOutlet UIImageView *imgNoDisplay;

- (IBAction)onClickBtnMenu:(id)sender;
- (IBAction)onClickBtnDistance:(id)sender;

// Outlet for detail view

@property (weak, nonatomic) IBOutlet UIView *viewForDetails;
//@property (weak, nonatomic) IBOutlet UIImageView *imgProfile;
@property (weak, nonatomic) IBOutlet UITextView *textViewForDescription;

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblVehicleYear;
@property (weak, nonatomic) IBOutlet UILabel *lblSource;
@property (weak, nonatomic) IBOutlet UILabel *lblSourceAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblDestination;
@property (weak, nonatomic) IBOutlet UILabel *lblDestinationAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;

@property (weak, nonatomic) IBOutlet UIButton *btnBidNow;
@property (weak, nonatomic) IBOutlet UIButton *btnSelection;

- (IBAction)onClickBtnBidNow:(id)sender;
- (IBAction)onClickBtnSelection:(id)sender;


// Outlet for Bid view

@property (weak, nonatomic) IBOutlet UILabel *lblPriceYouWantToCharge;
@property (weak, nonatomic) IBOutlet UILabel *lblPickUpTime;
@property (weak, nonatomic) IBOutlet UILabel *lblDescriptionForBid;

@property (weak, nonatomic) IBOutlet UITextField *txtPrice;

@property (weak, nonatomic) IBOutlet UIView *viewForBid;
@property (weak, nonatomic) IBOutlet UITextView *textViewForBidDescription;
@property (weak, nonatomic) IBOutlet UIView *viewForTimePicker;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePickerObj;


@property (weak, nonatomic) IBOutlet UIButton *btnClose;
@property (weak, nonatomic) IBOutlet UIButton *btnTime;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmitBid;
@property (weak, nonatomic) IBOutlet UIButton *btnPickerCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnPickerDone;

- (IBAction)onClickBtnCloseOfBidView:(id)sender;
- (IBAction)onClickBtnSubmitBid:(id)sender;
- (IBAction)onClickBtnTime:(id)sender;
- (IBAction)onClickBtnPickerCancel:(id)sender;
- (IBAction)onClickBtnPickerDone:(id)sender;
- (IBAction)onClickSelectDate:(id)sender;




// Outlet for pickerView

@property (weak, nonatomic) IBOutlet UIView *viewForPicker;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerObj;
@property (weak, nonatomic) IBOutlet UIButton *BtnPickerViewCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnPickerViewDone;
- (IBAction)onClickBtnPickerViewCancel:(id)sender;
- (IBAction)onClickBtnPickerViewDone:(id)sender;
-(void)CheckPush;


@end
