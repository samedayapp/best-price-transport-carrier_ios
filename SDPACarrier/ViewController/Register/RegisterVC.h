//
//  RegisterVC.h
//  SDPAClient
//
//  Created by Sapana Ranipa on 24/12/15.
//  Copyright © 2015 Sapana Ranipa. All rights reserved.
//

#import "BaseVC.h"

@interface RegisterVC : BaseVC<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    BOOL isProPicAdded;
}
@property (weak, nonatomic) IBOutlet UIImageView *imgProPic;
@property (weak, nonatomic) IBOutlet UIScrollView *ScrollObj;
@property (weak, nonatomic) IBOutlet UIView *viewForArroved;
@property (weak, nonatomic) IBOutlet UIView *viewForOtp;

@property (weak, nonatomic) IBOutlet UILabel *lblUploadPicture;
@property (weak, nonatomic) IBOutlet UILabel *lblApproveMessage;
@property (weak, nonatomic) IBOutlet UILabel *lblIAgree;
@property (weak, nonatomic) IBOutlet UILabel *lblLine;
@property (weak, nonatomic) IBOutlet UILabel *lblOtp;
@property (weak, nonatomic) IBOutlet UILabel *lblOtpMessage;

@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UITextField *txtUserName;
@property (weak, nonatomic) IBOutlet UITextField *txtOtp;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtConfirmPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtMobile;
@property (weak, nonatomic) IBOutlet UITextField *txtLicenseNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtPlateNumber;

@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIButton *btnCamera;
@property (weak, nonatomic) IBOutlet UIButton *btnGallery;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckBox;
@property (weak, nonatomic) IBOutlet UIButton *btnTermAndCondition;
@property (weak, nonatomic) IBOutlet UIButton *btnOK;

- (IBAction)onClickBtnCamera:(id)sender;
- (IBAction)onClickBtnGallery:(id)sender;
- (IBAction)onClickBtnBack:(id)sender;
- (IBAction)onClickBtnRegister:(id)sender;
- (IBAction)onClickBtnOK:(id)sender;
- (IBAction)onClickBtnCheckBox:(id)sender;
- (IBAction)onClickSubmitOtp:(id)sender;
@end
